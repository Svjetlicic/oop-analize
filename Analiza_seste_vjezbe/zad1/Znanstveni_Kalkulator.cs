// 1. zadatak
//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
//znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
//naprednih (sin, cos, log, sqrt...) operacija. 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Znanstveni_Kalkulator : Form
    {
        double a, b;

        public Znanstveni_Kalkulator()
        {
            InitializeComponent();
        }

        private void plus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
                MessageBox.Show("Pogresan unos.");
            else if (!double.TryParse(textBox2.Text, out b))
                MessageBox.Show("Pogresan unos.");
            else
                Rezultat.Text = (a + b).ToString();
        }

        private void minus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
                MessageBox.Show("Pogresan unos.");
            else if (!double.TryParse(textBox2.Text, out b))
                MessageBox.Show("Pogresan unos.");
            else
                Rezultat.Text = (a - b).ToString();
        }

        private void puta_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
                MessageBox.Show("Pogresan unos.");
            else if (!double.TryParse(textBox2.Text, out b))
                MessageBox.Show("Pogresan unos.");
            else
                Rezultat.Text = (a * b).ToString();
        }

        private void podijeljeno_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
                MessageBox.Show("Pogresan unos.");
            else if (!double.TryParse(textBox2.Text, out b))
                MessageBox.Show("Pogresan unos.");
            else
                Rezultat.Text = (a / b).ToString();
        }

        private void Sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
                MessageBox.Show("Pogresan unos broja.");
            else
                Rezultat.Text = Math.Sin(a).ToString();
        }

        private void Cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
                MessageBox.Show("Pogresan unos broja.");
            else
                Rezultat.Text = Math.Cos(a).ToString();
        }

        private void Tan_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
                MessageBox.Show("Pogresan unos broja.");
            else
                Rezultat.Text = Math.Tan(a).ToString();
        }

        private void Sqrt_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
                MessageBox.Show("Pogresan unos broja.");
            else
                Rezultat.Text = Math.Sqrt(a).ToString();
        }

        private void Log_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out a))
                MessageBox.Show("Pogresan unos broja.");
            else if (!double.TryParse(textBox2.Text, out b))
                MessageBox.Show("Pogresan unos baze.");
            else
                Rezultat.Text = Math.Log(a,b).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}