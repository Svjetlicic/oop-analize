namespace WindowsFormsApp1
{
    partial class Znanstveni_Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Quit = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.puta = new System.Windows.Forms.Button();
            this.podijeljeno = new System.Windows.Forms.Button();
            this.Sin = new System.Windows.Forms.Button();
            this.Cos = new System.Windows.Forms.Button();
            this.Sqrt = new System.Windows.Forms.Button();
            this.Tan = new System.Windows.Forms.Button();
            this.Log = new System.Windows.Forms.Button();
            this.Rezultat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Quit
            // 
            this.btn_Quit.Location = new System.Drawing.Point(688, 407);
            this.btn_Quit.Name = "btn_Quit";
            this.btn_Quit.Size = new System.Drawing.Size(100, 31);
            this.btn_Quit.TabIndex = 0;
            this.btn_Quit.Text = "IZLAZ\r\n";
            this.btn_Quit.UseVisualStyleBackColor = true;
            this.btn_Quit.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(115, 33);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(115, 100);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Prvi broj: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Drugi broj: ";
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(520, 30);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(58, 50);
            this.plus.TabIndex = 5;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(584, 30);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(58, 50);
            this.minus.TabIndex = 6;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // puta
            // 
            this.puta.Location = new System.Drawing.Point(648, 30);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(58, 50);
            this.puta.TabIndex = 7;
            this.puta.Text = "x";
            this.puta.UseVisualStyleBackColor = true;
            this.puta.Click += new System.EventHandler(this.puta_Click);
            // 
            // podijeljeno
            // 
            this.podijeljeno.Location = new System.Drawing.Point(712, 30);
            this.podijeljeno.Name = "podijeljeno";
            this.podijeljeno.Size = new System.Drawing.Size(58, 50);
            this.podijeljeno.TabIndex = 8;
            this.podijeljeno.Text = "/";
            this.podijeljeno.UseVisualStyleBackColor = true;
            this.podijeljeno.Click += new System.EventHandler(this.podijeljeno_Click);
            // 
            // Sin
            // 
            this.Sin.Location = new System.Drawing.Point(520, 115);
            this.Sin.Name = "Sin";
            this.Sin.Size = new System.Drawing.Size(58, 50);
            this.Sin.TabIndex = 9;
            this.Sin.Text = "Sin";
            this.Sin.UseVisualStyleBackColor = true;
            this.Sin.Click += new System.EventHandler(this.Sin_Click);
            // 
            // Cos
            // 
            this.Cos.Location = new System.Drawing.Point(584, 115);
            this.Cos.Name = "Cos";
            this.Cos.Size = new System.Drawing.Size(58, 50);
            this.Cos.TabIndex = 10;
            this.Cos.Text = "Cos";
            this.Cos.UseVisualStyleBackColor = true;
            this.Cos.Click += new System.EventHandler(this.Cos_Click);
            // 
            // Sqrt
            // 
            this.Sqrt.Location = new System.Drawing.Point(584, 204);
            this.Sqrt.Name = "Sqrt";
            this.Sqrt.Size = new System.Drawing.Size(58, 50);
            this.Sqrt.TabIndex = 11;
            this.Sqrt.Text = "Sqrt";
            this.Sqrt.UseVisualStyleBackColor = true;
            this.Sqrt.Click += new System.EventHandler(this.Sqrt_Click);
            // 
            // Tan
            // 
            this.Tan.Location = new System.Drawing.Point(648, 115);
            this.Tan.Name = "Tan";
            this.Tan.Size = new System.Drawing.Size(58, 50);
            this.Tan.TabIndex = 12;
            this.Tan.Text = "Tan";
            this.Tan.UseVisualStyleBackColor = true;
            this.Tan.Click += new System.EventHandler(this.Tan_Click);
            // 
            // Log
            // 
            this.Log.Location = new System.Drawing.Point(520, 204);
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(58, 50);
            this.Log.TabIndex = 13;
            this.Log.Text = "Log";
            this.Log.UseVisualStyleBackColor = true;
            this.Log.Click += new System.EventHandler(this.Log_Click);
            // 
            // Rezultat
            // 
            this.Rezultat.Location = new System.Drawing.Point(107, 218);
            this.Rezultat.Name = "Rezultat";
            this.Rezultat.Size = new System.Drawing.Size(184, 22);
            this.Rezultat.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "REZULTAT: ";
            // 
            // Znanstveni_Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Rezultat);
            this.Controls.Add(this.Log);
            this.Controls.Add(this.Tan);
            this.Controls.Add(this.Sqrt);
            this.Controls.Add(this.Cos);
            this.Controls.Add(this.Sin);
            this.Controls.Add(this.podijeljeno);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_Quit);
            this.Name = "Znanstveni_Kalkulator";
            this.Text = "Znanstveni kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Quit;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button puta;
        private System.Windows.Forms.Button podijeljeno;
        private System.Windows.Forms.Button Sin;
        private System.Windows.Forms.Button Cos;
        private System.Windows.Forms.Button Sqrt;
        private System.Windows.Forms.Button Tan;
        private System.Windows.Forms.Button Log;
        private System.Windows.Forms.TextBox Rezultat;
        private System.Windows.Forms.Label label3;
    }
}
