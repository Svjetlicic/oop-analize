namespace _2.zadatak
{
    partial class Vjesalo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.broj_slova = new System.Windows.Forms.Label();
            this.broj_pokusaja = new System.Windows.Forms.Label();
            this.Izlaz = new System.Windows.Forms.Button();
            this.Ucitaj = new System.Windows.Forms.Button();
            this.slovo = new System.Windows.Forms.TextBox();
            this.Unos = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // broj_slova
            // 
            this.broj_slova.AutoSize = true;
            this.broj_slova.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.broj_slova.Location = new System.Drawing.Point(106, 250);
            this.broj_slova.Name = "broj_slova";
            this.broj_slova.Size = new System.Drawing.Size(117, 29);
            this.broj_slova.TabIndex = 0;
            this.broj_slova.Text = "broj slova";
            // 
            // broj_pokusaja
            // 
            this.broj_pokusaja.AutoSize = true;
            this.broj_pokusaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.broj_pokusaja.Location = new System.Drawing.Point(108, 311);
            this.broj_pokusaja.Name = "broj_pokusaja";
            this.broj_pokusaja.Size = new System.Drawing.Size(158, 29);
            this.broj_pokusaja.TabIndex = 1;
            this.broj_pokusaja.Text = "broj pokušaja";
            // 
            // Izlaz
            // 
            this.Izlaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Izlaz.Location = new System.Drawing.Point(672, 374);
            this.Izlaz.Name = "Izlaz";
            this.Izlaz.Size = new System.Drawing.Size(109, 51);
            this.Izlaz.TabIndex = 2;
            this.Izlaz.Text = "Izlaz";
            this.Izlaz.UseVisualStyleBackColor = true;
            this.Izlaz.Click += new System.EventHandler(this.Izlaz_Click);
            // 
            // Ucitaj
            // 
            this.Ucitaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Ucitaj.Location = new System.Drawing.Point(688, 55);
            this.Ucitaj.Name = "Ucitaj";
            this.Ucitaj.Size = new System.Drawing.Size(92, 45);
            this.Ucitaj.TabIndex = 3;
            this.Ucitaj.Text = "Učitaj";
            this.Ucitaj.UseVisualStyleBackColor = true;
            this.Ucitaj.Click += new System.EventHandler(this.Ucitaj_Click);
            // 
            // slovo
            // 
            this.slovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.slovo.Location = new System.Drawing.Point(40, 33);
            this.slovo.Name = "slovo";
            this.slovo.Size = new System.Drawing.Size(64, 45);
            this.slovo.TabIndex = 4;
            // 
            // Unos
            // 
            this.Unos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Unos.Location = new System.Drawing.Point(146, 33);
            this.Unos.Name = "Unos";
            this.Unos.Size = new System.Drawing.Size(100, 45);
            this.Unos.TabIndex = 5;
            this.Unos.Text = "Unos";
            this.Unos.UseVisualStyleBackColor = true;
            this.Unos.Click += new System.EventHandler(this.Unos_Click);
            // 
            // Vjesalo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Unos);
            this.Controls.Add(this.slovo);
            this.Controls.Add(this.Ucitaj);
            this.Controls.Add(this.Izlaz);
            this.Controls.Add(this.broj_pokusaja);
            this.Controls.Add(this.broj_slova);
            this.Name = "Vjesalo";
            this.Text = "Vjesalo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label broj_slova;
        private System.Windows.Forms.Label broj_pokusaja;
        private System.Windows.Forms.Button Izlaz;
        private System.Windows.Forms.Button Ucitaj;
        private System.Windows.Forms.TextBox slovo;
        private System.Windows.Forms.Button Unos;
    }
}