//2. zadatak

//Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u
//svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
//funkcionalnost koju biste očekivali od takve igre. Nije nužno crtati vješala,
//dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2.zadatak
{
    public partial class Vjesalo : Form
    {
        Random rand = new Random();
        List<string> list = new List<string>();
        int pokusaj;
        string rijec, skrivenaRijec;
        public void reset()
        {
            rijec = list[rand.Next(0, list.Count - 1)];
            skrivenaRijec = new string('*', rijec.Length);
            broj_slova.Text = skrivenaRijec;
            pokusaj = 5;
            broj_pokusaja.Text = pokusaj.ToString();
        }

        public Vjesalo()
        {
            InitializeComponent();
        }

        private void Ucitaj_Click(object sender, EventArgs e)
        {
            string linija;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@"D:\Users\Korisnik\Desktop\zadatak2.txt")) {
                while ((linija = reader.ReadLine()) != null)
                {
                    list.Add(linija);
                }
                reset();
            }
        }

        private void Unos_Click(object sender, EventArgs e)
        {
            if (slovo.Text.Length == 1)
            {
                if (rijec.Contains(slovo.Text))
                {
                    string temp_rijec = rijec;
                    while (temp_rijec.Contains(slovo.Text))
                    {
                        int index = temp_rijec.IndexOf(slovo.Text);
                        StringBuilder builder = new StringBuilder

                        (temp_rijec);

                        builder[index] = '*';
                        temp_rijec = builder.ToString();
                        StringBuilder builder2 = new StringBuilder(skrivenaRijec);
                        builder2[index] = Convert.ToChar(slovo.Text);
                        skrivenaRijec = builder2.ToString();
                    }
                    broj_slova.Text = skrivenaRijec;
                    if (skrivenaRijec == rijec)
                    {
                        MessageBox.Show("Pogodili ste riječ!");
                        reset();
                    }
                }
                else
                {
                    pokusaj--;
                    broj_pokusaja.Text = pokusaj.ToString();
                    if (pokusaj <= 0)
                    {
                        MessageBox.Show("Nažalost, nemate više pokušaja.");
                        reset();
                    }
                }
            }
            else if (slovo.Text.Length > 1)
            {
                if (rijec == slovo.Text)
                {
                    MessageBox.Show("Pogodili ste riječ!");
                    reset();
                }
                else
                {
                    pokusaj--;
                    broj_pokusaja.Text = pokusaj.ToString();
                    if (pokusaj <= 0)
                    {
                        MessageBox.Show("Nažalost, nemate više pokušaja.");
                        reset();
                    }
                }
            }
        }

        private void Izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}